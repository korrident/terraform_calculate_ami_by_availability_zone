# output "debug_all_details" {
#   value = "${data.external.subnet_available_for_ami}"
# }

output "ami_by_subnet" {
  value = "${local.uniq_answers_filtered}"
}
output "ami" {
  value = "${data.aws_ami.latest_ami.id}"
}
output "subnets_id" {
  value = "${local.uniq_subnet_filtered}"
}
