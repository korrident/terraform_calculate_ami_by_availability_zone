# Module calculate_ami_by_availability_zone

## Context

You are using terraform on AWS.\
You want to input in your module requirements on the AMI + the subnets available.\
This module is more a function that returns you the ami found and the eligible subnets.

## Requirements

You need to be able to execute the script:
- bash
- jq
- aws cli

## Goal

Create a missing data in terraform:

- given an ec2 instance data type
- given a list of subnets
- find an ami
- find the ami/subnet matching

## Using or contributing

You can use / improve this code.