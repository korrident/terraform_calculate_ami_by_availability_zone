#!/bin/bash

## creating own data source https://www.terraform.io/docs/providers/external/data_source.html
## good example https://gist.github.com/irvingpop/968464132ded25a206ced835d50afa6b
## good example https://medium.com/@jmarhee/using-external-data-sources-with-terraform-c3de27388192

function error_exit(){
	echo "$1" 1>&2
	exit 1
}

function check_deps() {
  test -f $(which jq) || error_exit "jq command not detected in path, please install it"
}

function parse_input() {
    eval "$(jq -r '@sh "export AMI=\(.ami)  INSTANCE_TYPE=\(.type)  REGION=\(.region)  SUBNET_ID=\(.subnet)  PROFILE=\(.profile)   "')"

    ([ -z "${AMI}" ] || [ "${AMI}" == "null" ]) && error_exit "missing AMI"
    ([ -z "${INSTANCE_TYPE}" ] || [ "${INSTANCE_TYPE}" == "null" ]) && error_exit "missing INSTANCE_TYPE"
    ([ -z "${REGION}" ] || [ "${REGION}" == "null" ]) && error_exit "missing REGION"
    ([ -z "${SUBNET_ID}" ] || [ "${SUBNET_ID}" == "null" ])  && error_exit "missing SUBNET_ID"
    ([ -z "${PROFILE}" ] || [ "${PROFILE}" == "null" ])  && error_exit "missing PROFILE"
}

function operation(){
    # ok when receving error: "An error occurred (DryRunOperation) when calling the RunInstances operation: Request would have succeeded, but DryRun flag is set."

CMD="aws --region=${REGION} ec2 run-instances \
--instance-type ${INSTANCE_TYPE} \
--image-id ${AMI} \
--subnet-id ${SUBNET_ID} \
--profile ${PROFILE} \
--dry-run" 

    # echo `date` >> /tmp/$0_debug.log
    # echo "${CMD}" >> /tmp/$0_debug.log

    RES=`${CMD} 2>&1`   
    # echo "${RES}" >> /tmp/$0_debug.log

    echo ${RES} | grep -c "DryRunOperation" > /dev/null 
    OP_EXIT_CODE=$?
    
    # echo "OP_EXIT_CODE=${OP_EXIT_CODE}" >> /tmp/$0_debug.log
}


function return_output() {
  if [[ ${OP_EXIT_CODE} -ne 0 ]]; then 
    echo '{}'
    return
  fi  
  jq -n \
    --arg ami "$AMI" \
    --arg subnet "$SUBNET_ID" \
    '{"ami":$ami,"subnet":$subnet}'
    return
}

 check_deps 
 parse_input 
 operation 
 return_output
