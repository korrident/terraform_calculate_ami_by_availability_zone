provider "aws" {
  version = "~> 2.19"
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}


data "aws_ami" "latest_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ami_path}"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["${var.ami_owner}"]
}

data "external" "subnet_available_for_ami" {
  count = "${length(var.subnets_id)}"

  program = ["bash", "${path.module}/check_subnet_ami.bash"]

  query = {
    ami     = "${data.aws_ami.latest_ami.id}"
    type    = "${var.instance_type}"
    subnet  = "${var.subnets_id[count.index]}"
    region  = "${var.aws_region}"
    profile = "${var.aws_profile}"
  }
}

locals {
  uniq_answers = "${distinct(data.external.subnet_available_for_ami.*.result)}"

  uniq_answers_filtered = [
    for a in local.uniq_answers :
    a if length(a) != 0
  ]

  uniq_subnet_filtered = [
    for a in local.uniq_answers_filtered :
    a.subnet
  ]
}
