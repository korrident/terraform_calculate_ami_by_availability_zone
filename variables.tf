
variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  type        = string
}

variable "subnets_id" {
  description = "reference to the VPC->Subnets"
  type        = list(string)
}

variable "instance_type" {
  description = "size of the instance (e.g. t2.nano)"
  type        = string
}
variable "ami_owner" {
  description = "who created this image (e.g. 099720109477 for Canonical)"
  type        = string
}
variable "ami_path" {
  description = "path of the image (e.g. ubuntu/images/hvm-ssd/ubuntu-disco-19.04-amd64-server-*)"
  type        = string
}
variable "aws_profile" {
  description = "The AWS account to use"
  type        = string
}
